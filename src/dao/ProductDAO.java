package dao;

import db.DBConnection;
import model.ProductDTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ProductDAO {
    private Connection connection;

    public ArrayList<ProductDTO> getAll() throws Exception{
        connection= DBConnection.getDBConnection().getConnection();
        Statement stm=connection.createStatement();
        String sql="SELECT * FROM product WHERE Step1_time>CURRENT_TIMESTAMP()-INTERVAL '1:1' MINUTE_SECOND";
        ResultSet rst=stm.executeQuery(sql);
        ArrayList<ProductDTO> productList=new ArrayList<>();
        while (rst.next()) {

            productList.add(new ProductDTO(
                    rst.getString(1),
                    rst.getInt(2),
                    rst.getInt(3),
                    rst.getInt(4),
                    rst.getInt(5),
                    rst.getInt(6),
                    rst.getString(7),
                    rst.getString(8),
                    rst.getString(9),
                    rst.getString(10),
                    rst.getString(11)

            ));

        }

        return productList;

    }
    public ArrayList<ProductDTO> getAll2() throws Exception{
        connection= DBConnection.getDBConnection().getConnection();
        Statement stm=connection.createStatement();
        String sql="SELECT * FROM product WHERE Step1_time>CURRENT_TIMESTAMP()-INTERVAL '5:1' MINUTE_SECOND";
        ResultSet rst=stm.executeQuery(sql);
        ArrayList<ProductDTO> productList=new ArrayList<>();
        while (rst.next()) {

            productList.add(new ProductDTO(
                    rst.getString(1),
                    rst.getInt(2),
                    rst.getInt(3),
                    rst.getInt(4),
                    rst.getInt(5),
                    rst.getInt(6),
                    rst.getString(7),
                    rst.getString(8),
                    rst.getString(9),
                    rst.getString(10),
                    rst.getString(11)

            ));

        }

        return productList;

    }

}
