package Controller;



import dao.ProductDAO;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import model.ProductDTO;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DashboardController implements Initializable{
    @FXML
    private BarChart<?, ?> barProduction1;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis Y;

    @FXML
    private LineChart<?, ?> lineError1;

    @FXML
    private CategoryAxis x1;

    @FXML
    private NumberAxis y1;

    @FXML
    private Button btn1;

    @FXML
    private BarChart<?, ?> barProduction2;

    @FXML
    private CategoryAxis x2;

    @FXML
    private NumberAxis y2;

    @FXML
    private LineChart<?, ?> lineError2;

    @FXML
    private CategoryAxis x3;

    @FXML
    private NumberAxis y3;


    ProductDAO productDAO=new ProductDAO();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
            startTask();
    }




    public void startTask()

    {


        Runnable task = new Runnable()

        {

            public void run()

            {

                runTask();
                runTask2();

            }

        };





        Thread backgroundThread = new Thread(task);



        backgroundThread.setDaemon(true);



        backgroundThread.start();

    }



    public void runTask()

    {

        while (true){

            try

            {


                Platform.runLater(new Runnable()

                {

                    @Override

                    public void run()

                    {

                        realTimeProcessing();


                    }

                });





                Thread.sleep(60000);

            }

	            catch (InterruptedException e)

            {

                e.printStackTrace();

            }

        }

    }
    public void runTask2()

    {

        while (true){

            try

            {


                Platform.runLater(new Runnable()

                {

                    @Override

                    public void run()

                    {


                        averageProcessing();

                    }

                });





                Thread.sleep(300000);//5 minutes

            }

            catch (InterruptedException e)

            {

                e.printStackTrace();

            }

        }

    }
    public void averageProcessing(){
        int productCount1=0,productCount2=0,productCount3=0,productCount4=0,productCount5=0;
        int faultCount1=0,faultCount2=0,faultCount3=0,faultCount4=0,faultCount5=0;
        double errorPercentage1,errorPercentage2,errorPercentage3,errorPercentage4,errorPercentage5;

        ArrayList<ProductDTO> productList= null;
        try {
            productList = productDAO.getAll();
            for (ProductDTO product : productList) {
                if(product.getStep1()!=2){
                    productCount1++;
                    if(product.getStep1()==1){
                        faultCount1++;
                    }
                }
                if(product.getStep2()!=2){
                    productCount2++;
                    if(product.getStep2()==1){
                        faultCount2++;
                    }
                }
                if(product.getStep3()!=2){
                    productCount3++;
                    if(product.getStep3()==1){
                        faultCount3++;
                    }
                }
                if(product.getStep4()!=2){
                    productCount4++;
                    if(product.getStep4()==1){
                        faultCount4++;
                    }
                }
                if(product.getStep5()!=2){
                    productCount5++;
                    if(product.getStep5()==1){
                        faultCount5++;
                    }
                }

            }

            XYChart.Series set1=new XYChart.Series<>();
            set1.getData().add(new XYChart.Data("1", productCount1));
            set1.getData().add(new XYChart.Data("2", productCount2));
            set1.getData().add(new XYChart.Data("3", productCount3));
            set1.getData().add(new XYChart.Data("4", productCount4));
            set1.getData().add(new XYChart.Data("5", productCount5));
            barProduction2.getData().clear();
            barProduction2.getData().addAll(set1);

            errorPercentage1=((double)faultCount1/(double)productCount1)*100;
            errorPercentage2=((double)faultCount2/(double)productCount2)*100;
            errorPercentage3=((double)faultCount3/(double)productCount3)*100;
            errorPercentage4=((double)faultCount4/(double)productCount4)*100;
            errorPercentage5=((double)faultCount5/(double)productCount5)*100;



            XYChart.Series set2=new XYChart.Series<>();
            set2.getData().add(new XYChart.Data("1", errorPercentage1));
            set2.getData().add(new XYChart.Data("2", errorPercentage2));
            set2.getData().add(new XYChart.Data("3", errorPercentage3));
            set2.getData().add(new XYChart.Data("4", errorPercentage4));
            set2.getData().add(new XYChart.Data("5", errorPercentage5));
            lineError2.getData().clear();
            lineError2.getData().addAll(set2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void realTimeProcessing(){
        int productCount1=0,productCount2=0,productCount3=0,productCount4=0,productCount5=0;
        int faultCount1=0,faultCount2=0,faultCount3=0,faultCount4=0,faultCount5=0;
        double errorPercentage1,errorPercentage2,errorPercentage3,errorPercentage4,errorPercentage5;

        ArrayList<ProductDTO> productList= null;
        try {
            productList = productDAO.getAll();
            for (ProductDTO product : productList) {
                if(product.getStep1()!=2){
                    productCount1++;
                    if(product.getStep1()==1){
                        faultCount1++;
                    }
                }
                if(product.getStep2()!=2){
                    productCount2++;
                    if(product.getStep2()==1){
                        faultCount2++;
                    }
                }
                if(product.getStep3()!=2){
                    productCount3++;
                    if(product.getStep3()==1){
                        faultCount3++;
                    }
                }
                if(product.getStep4()!=2){
                    productCount4++;
                    if(product.getStep4()==1){
                        faultCount4++;
                    }
                }
                if(product.getStep5()!=2){
                    productCount5++;
                    if(product.getStep5()==1){
                        faultCount5++;
                    }
                }

            }

            XYChart.Series set1=new XYChart.Series<>();
            set1.getData().add(new XYChart.Data("1", productCount1));
            set1.getData().add(new XYChart.Data("2", productCount2));
            set1.getData().add(new XYChart.Data("3", productCount3));
            set1.getData().add(new XYChart.Data("4", productCount4));
            set1.getData().add(new XYChart.Data("5", productCount5));
            barProduction1.getData().clear();
            barProduction1.getData().addAll(set1);

            errorPercentage1=((double)faultCount1/(double)productCount1)*100;
            errorPercentage2=((double)faultCount2/(double)productCount2)*100;
            errorPercentage3=((double)faultCount3/(double)productCount3)*100;
            errorPercentage4=((double)faultCount4/(double)productCount4)*100;
            errorPercentage5=((double)faultCount5/(double)productCount5)*100;



            XYChart.Series set2=new XYChart.Series<>();
            set2.getData().add(new XYChart.Data("1", errorPercentage1));
            set2.getData().add(new XYChart.Data("2", errorPercentage2));
            set2.getData().add(new XYChart.Data("3", errorPercentage3));
            set2.getData().add(new XYChart.Data("4", errorPercentage4));
            set2.getData().add(new XYChart.Data("5", errorPercentage5));
            lineError1.getData().clear();
            lineError1.getData().addAll(set2);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}
