package model;

public class ProductDTO {
    private String pid;
    private int step1;
    private int step2;
    private int step3;
    private int step4;
    private int step5;
    private String step1_time;
    private String step2_time;
    private String step3_time;
    private String step4_time;
    private String step5_time;

    public ProductDTO(String pid, int step1, int step2, int step3, int step4, int step5, String step1_time, String step2_time, String step3_time, String step4_time, String step5_time) {
        this.pid = pid;
        this.step1 = step1;
        this.step2 = step2;
        this.step3 = step3;
        this.step4 = step4;
        this.step5 = step5;
        this.step1_time = step1_time;
        this.step2_time = step2_time;
        this.step3_time = step3_time;
        this.step4_time = step4_time;
        this.step5_time = step5_time;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getStep1() {
        return step1;
    }

    public void setStep1(int step1) {
        this.step1 = step1;
    }

    public int getStep2() {
        return step2;
    }

    public void setStep2(int step2) {
        this.step2 = step2;
    }

    public int getStep3() {
        return step3;
    }

    public void setStep3(int step3) {
        this.step3 = step3;
    }

    public int getStep4() {
        return step4;
    }

    public void setStep4(int step4) {
        this.step4 = step4;
    }

    public int getStep5() {
        return step5;
    }

    public void setStep5(int step5) {
        this.step5 = step5;
    }

    public String getStep1_time() {
        return step1_time;
    }

    public void setStep1_time(String step1_time) {
        this.step1_time = step1_time;
    }

    public String getStep2_time() {
        return step2_time;
    }

    public void setStep2_time(String step2_time) {
        this.step2_time = step2_time;
    }

    public String getStep3_time() {
        return step3_time;
    }

    public void setStep3_time(String step3_time) {
        this.step3_time = step3_time;
    }

    public String getStep4_time() {
        return step4_time;
    }

    public void setStep4_time(String step4_time) {
        this.step4_time = step4_time;
    }

    public String getStep5_time() {
        return step5_time;
    }

    public void setStep5_time(String step5_time) {
        this.step5_time = step5_time;
    }
}
