package db;

import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static DBConnection dbconnection;
    private java.sql.Connection conn;

    private DBConnection() throws SQLException,ClassNotFoundException{
        Class.forName("com.mysql.jdbc.Driver");
        conn= DriverManager.getConnection("jdbc:mysql://localhost/efactory","root","");
    }
    public static DBConnection getDBConnection()throws SQLException,ClassNotFoundException{
        if (dbconnection==null){
            dbconnection=new DBConnection();
        }
        return dbconnection;

    }
    public java.sql.Connection getConnection(){
        return conn;
    }
}
