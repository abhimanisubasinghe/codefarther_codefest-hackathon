/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httpserver;

/**
 *
 * @author Sanjali Nanayakkara
 */
public interface ProductDAO {
    boolean addProduct(Product product);
    boolean deleteProduct(int PID);
    Product searchProduct(int PID);
    boolean updateProduct(Product product);
}
