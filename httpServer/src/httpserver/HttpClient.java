
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpClient {

	private static final String USER_AGENT = "Mozilla/5.0";

	private static final String GET_URL = "http://localhost:8084/ServerWeb/HttpServer";

	private static final String POST_URL = "http://localhost:8084/ServerWeb/HttpServer";

	private static final String POST_PARAMS = "userName=Pankaj";

	public static void main(String[] args) throws IOException, JSONException {

		/*sendGET();
		System.out.println("GET DONE");*/
		sendPOST();
		System.out.println("POST DONE");
	}

	private static void sendGET() throws IOException {
		URL obj = new URL(GET_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("GET request not worked");
		}

	}

	private static void sendPOST() throws IOException, JSONException {
		URL obj = new URL(POST_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
                int start=0,end=100;
                
                for(int y=0;y<5;y++){
                for(int x=1;x<6;x++){
                    JSONObject jobj = new JSONObject();
                    
                    String s=String.valueOf(x);
                    
		jobj.put("StepId", s);
		jobj.put("Time", "Now");
                    
		JSONArray id = new JSONArray();
                for(int i=start;i<end;i++){
                    JSONObject pro = new JSONObject();
                    Random rand = new Random();
                    int  n = rand.nextInt(50) + 1;
                    if(n%2==1){
                        pro.put("p"+i,1);
                    }
                    else if(n%2==0){
                        pro.put("p"+i,0);
                    }
                    id.put(pro);
                }   
		jobj.put("pid", id);
                jobj.put("start", 0);
                jobj.put("end",100);
                
                
		//os.write(POST_PARAMS.getBytes());
                
                String j=jobj.toString();
                os.write(j.getBytes());
		
                }
                start=start+100;
                end=end+100;
                }
                
		// For POST only - END
                   os.flush();
		os.close();
		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("POST request not worked");
		}
	}

}
