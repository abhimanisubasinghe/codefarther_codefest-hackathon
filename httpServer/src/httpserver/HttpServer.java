
import httpserver.Product;
import httpserver.ProductDAO;
import httpserver.ProductDAOImpl;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jdk.nashorn.internal.parser.JSONParser;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.*;

/**
 *
 * @author kirut
 */
@WebServlet(urlPatterns = {"/HttpServer"})
public class HttpServer extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int i=Integer.parseInt(request.getParameter("t1"));
        int j=Integer.parseInt(request.getParameter("t2"));
        int k=i+j;
        PrintWriter out=response.getWriter();
        out.println(k);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         PrintWriter out=response.getWriter();
         String json = getBody(request);
         //out.println(json);
         
         
        try {
            

            JSONObject jsonobj = new JSONObject(json);
            
            int stepId = Integer.valueOf(jsonobj.getString("StepId"));
            out.println("step id is "+stepId);
           
            
            int start=jsonobj.getInt("start");
            int end=jsonobj.getInt("end");
            
            JSONArray id2=jsonobj.getJSONArray("pid");
            
            for (int i = start; i <end-1; i++) {
                JSONObject jsonobject = id2.getJSONObject(i);
                int fault = jsonobject.getInt("p"+i);
                Date date=new Date();
                //date=LocalDateTime.now();
                Product pr=new Product("p"+i,fault,date,stepId);
                
                ProductDAO pp=new ProductDAOImpl(); //interface and its implement class
                
                pp.addProduct(pr);
                
                out.println("p"+i+" is "+fault);
                // "p"+i === productid
                // fault== fault
                //stepid == step
            } 

        } catch (JSONException ex) {
            Logger.getLogger(HttpServer.class.getName()).log(Level.SEVERE, null, ex);
            out.println("Eroor");
        }
        
        /*out.println(json);
        out.println("server response");*/
        
    }
    
    
    public static String getBody(HttpServletRequest request) throws IOException {

    String body = null;
    StringBuilder stringBuilder = new StringBuilder();
    BufferedReader bufferedReader = null;

    try {
        InputStream inputStream = request.getInputStream();
        if (inputStream != null) {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            char[] charBuffer = new char[128];
            int bytesRead = -1;
            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                stringBuilder.append(charBuffer, 0, bytesRead);
            }
        } else {
            stringBuilder.append("");
        }
    } catch (IOException ex) {
        throw ex;
    } finally {
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                throw ex;
            }
        }
    }

    body = stringBuilder.toString();
    return body;
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
