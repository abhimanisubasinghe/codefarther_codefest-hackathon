
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;



public class HttpServer2 {
    
    public static void main(String[] args) throws Exception {
    HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
    server.createContext("/requests", new MyHandler());
    server.setExecutor(null); // creates a default executor
    server.start();
    }

    static class MyHandler implements HttpHandler {
    public void handle(HttpExchange t) throws IOException {
        String response = "hello world";
        t.sendResponseHeaders(200, response.length());
        System.out.println(response);
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
         }
    }
    
   
}