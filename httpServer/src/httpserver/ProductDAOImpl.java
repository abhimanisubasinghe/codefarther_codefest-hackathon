/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httpserver;

import db.DBConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.omg.CosNaming.NamingContextExtHelper.insert;

/**
 *
 * @author Sanjali Nanayakkara
 */
public class ProductDAOImpl implements ProductDAO {

    boolean result = false;
    Connection connection;
    
    ProductDAOImpl() throws ClassNotFoundException, SQLException{
        connection = DBConnection.getDBConection().getConnection();
    }
    
    @Override
    public boolean addProduct(Product product) {
        if(product.stepId==1){
            String insert = "INSERT INTO PRODUCT(PID,Step1,Timestamp1) VALUES"
                + "("+product.PID
                + ","+product.value
                + ","+product.timeStamp
                + ")";
        }
        
        if(product.stepId==2){
            String insert = "INSERT INTO PRODUCT(Step2,Timestamp2) VALUES"
                + "("+product.value
                + ","+product.timeStamp
                + ")"+ "WHERE PID="+"'"+product.PID+"'";
        }
        
         if(product.stepId==3){
            String insert = "INSERT INTO PRODUCT(Step3,Timestamp3) VALUES"
                + "("+product.value
                + ","+product.timeStamp
                + ")" + "WHERE PID="+"'"+product.PID+"'";
        }
         
         
          if(product.stepId==4){
            String insert = "INSERT INTO PRODUCT(Step4,Timestamp4) VALUES"
                + "("+product.value
                + ","+product.timeStamp
                + ")" + "WHERE PID="+"'"+product.PID+"'";
        }
          
          
         if(product.stepId==5){
            String insert = "INSERT INTO PRODUCT(Step5,Timestamp5) VALUES"
                + "("+product.value
                + ","+product.timeStamp
                + ")" + "WHERE PID="+"'"+product.PID+"'";
        }
        try {
            Statement stm = connection.createStatement();
            result = stm.execute(insert);
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public boolean deleteProduct(int PID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Product searchProduct(int PID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateProduct(Product product) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
